<?php
/**
 * Created by PhpStorm.
 * User: stana
 * Date: 04.12.2018
 * Time: 13:43
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DecodeFileRequest
 * @package App\Http\Requests
 */
class DecodeFileRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'inputFile' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'inputFile.required' => 'Vložte prosím soubor pro dekódování!',
        ];
    }
}