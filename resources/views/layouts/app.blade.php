<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aritmetické kódování</title>

    <!-- Bootstrap core CSS-->
    <link href="{{asset("css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("css/main.css")}}" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-white" id="page-top">
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="{{url ('/')}}">
        <img src="{{ url("images/tool_1_white.png") }}" width="30" height="30" class="d-inline-block align-top"> Tool
        For Aritmethic Coding! </a>
</nav>

<div class="content-wrapper">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>

<script src="{{url("js/jquery.js")}}"></script>
<script src="{{url("js/alertFadeOut.js")}}"></script>
</body>
</html>
